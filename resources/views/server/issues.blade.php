@extends('layouts.app')

@include('layouts.server.menu')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Viewing {{ $server[0]->name }} issues</div>

                    <div class="panel-body">
                        <p>Issues are things that need fixing on the server you're viewing. Each issue is color coded
                            according to severity.</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
