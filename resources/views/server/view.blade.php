@extends('layouts.app')

@include('layouts.server.menu')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Viewing <?php echo $server[0]->name; ?></div>

                    <div class="panel-body">
                        <?php echo $server[0]->desc; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
