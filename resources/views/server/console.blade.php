@extends('layouts.app')

@include('layouts.server.menu')

@section('content')
    <?php

    require(base_path('vendor\phpseclib\phpseclib\phpseclib\bootstrap.php'));
    require(base_path('vendor\phpseclib\phpseclib\phpseclib\File\ANSI.php'));
    require(base_path('vendor\phpseclib\phpseclib\phpseclib\Net\SSH2.php'));

    ?>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Viewing <?php echo $server[0]->name; ?> console</div>
                    <div class="panel-body">
                        <p>Unfortunately, due to security issues you can only view the console from here. We hope to add
                            the ability to send commands soon.</p>
                        <?php

                        use phpseclib\Net\SSH2;
                        use phpseclib\File\ANSI;
                        $ansi = new phpseclib\File\ANSI();

                        $ssh = new phpseclib\Net\SSH2('91.121.84.100');
                        if (!$ssh->login('root', 'Penguin1984')) {
                            exit('Login Failed');
                        }

                        $ssh->write('screen -x ' . $server[0]->screen_name . "\n");
                        $ssh->setTimeout(6);
                        $ansi->appendString($ssh->read());
                        echo $ansi->getScreen();
                        $ssh->disconnect();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
