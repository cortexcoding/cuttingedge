@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Viewing All Servers</div>

                    <div class="panel-body">
                        <table class="table table-hover">
                            <thead>
                            <td>ID</td>
                            <td>Name</td>
                            <td>Description</td>
                            <td></td>
                            </thead>
                            @foreach ($servers as $s)
                                <tr>
                                    <td>{{ $s->id }}</td>
                                    <td>{{ $s->name }}</td>
                                    <td>{{ $s->desc }}</td>
                                    <td><a href="server/{{  $s->name }} ">View</a></td>
                                </tr>
                            @endforeach
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
