<ul class="menu">
    <li><a href="/server/{{ $server[0]->name }}">Overview</a></li>
    <li><a href="/server/{{ $server[0]->name }}/issues">Issues</a></li>
    <li><a href="/server/{{ $server[0]->name }}/console">Console</a></li>
    <li><a href="/server/{{ $server[0]->name }}/files">Files</a></li>
    <li style="border: none; background: none" class="seperator"></li>
    <li style="border-left-color: #009E47; "><a href="/server/{{ $server[0]->name }}/functions/start">Start</a></li>
    <li style="border-left-color: #FFA135; "><a href="/server/{{ $server[0]->name }}/functions/restart">Restart</a></li>
    <li style="border-left-color: red;"><a href="/server/{{ $server[0]->name }}/functions/stop">Stop</a></li>
</ul>