@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">View Profile - {{Auth::user()->name }}</div>

                    <div class="panel-body">
                        <div class="panel panel-danger">
                            <div class="panel-heading">
                                <h3 class="panel-title">Woah!</h3>
                            </div>
                            <div class="panel-body">
                                We know there isn't much here at the moment. Sorry. We're working on allowing you too
                                see more soon. We promise.
                            </div>
                        </div>
                        <h1>Hey, {{ Auth::user()->name }}</h1>
                        <p>Email: {{ Auth::user()->email }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
