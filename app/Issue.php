<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Issue extends Model
{
    public function Server()
    {
        return $this->belongsTo('App/Server');
    }
}
