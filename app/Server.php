<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    public function Issue()
    {
        return $this->hasMany('App/Issues');
    }
}
