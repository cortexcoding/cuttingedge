<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ServerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $servers = DB::table('servers')->paginate(15);

        return view('server.index', ['servers' => $servers]);
    }

    public function view($name)
    {
        $server = DB::select('select * from servers where name = ?', [$name]);

        return view('server.view', ['server' => $server]);
    }

    public function viewIssues($name)
    {
        $server = DB::select('select * from servers where name = ?', [$name]);

        return view('server.issues', ['server' => $server]);
    }

    public function viewConsole($name)
    {
        $server = DB::select('select * from servers where name = ?', [$name]);

        return view('server.console', ['server' => $server]);
    }

    public function startServer($name)
    {
        $server = DB::select('select * from servers where name = ?', [$name]);

        require(base_path('vendor\phpseclib\phpseclib\phpseclib\bootstrap.php'));
        require(base_path('vendor\phpseclib\phpseclib\phpseclib\File\ANSI.php'));
        require(base_path('vendor\phpseclib\phpseclib\phpseclib\Net\SSH2.php'));


        // New Line
        $ssh = new phpseclib\Net\SSH2('91.121.84.100');
        if (!$ssh->login('root', 'Penguin1984')) {
            exit('Login Failed');
        }

        $ssh->write('screen -x test_screen \n');
        $ssh->setTimeout(6);
        $ssh->disconnect();

        return view('server.view', [$server => 'server']);
    }
}
