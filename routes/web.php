<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Routes for Debug
// TODO: Remove before deployment
Route::get('routes', function () {
    $routeCollection = Route::getRoutes();

    echo "<table style='width:100%'>";
    echo "<tr>";
    echo "<td width='10%'><h4>HTTP Method</h4></td>";
    echo "<td width='10%'><h4>Route</h4></td>";
    echo "<td width='80%'><h4>Corresponding Action</h4></td>";
    echo "</tr>";
    foreach ($routeCollection as $value) {
        echo "<tr>";
        echo "<td>" . $value->getMethods()[0] . "</td>";
        echo "<td>" . $value->getPath() . "</td>";
        echo "<td>" . $value->getActionName() . "</td>";
        echo "</tr>";
    }
    echo "</table>";
});

// General Routes
Route::get('/home', 'HomeController@index');
Route::get('/', function () {
    return view('welcome');
});
Route::get('/about', function () {
    return view('about');
});

// Routes for user managements
Auth::routes();
Route::get('/profile', function () {
    return view('profile');
})->middleware('auth');


// Routes for the servers
Route::get('/servers', 'ServerController@index');
Route::get('/server/{id}', 'ServerController@view');
Route::get('/server/{id}/issues', 'ServerController@viewIssues');
Route::get('/server/{id}/console', 'ServerController@viewConsole');
Route::get('/server/{id}/function/start', 'ServerController@startServer');

